﻿using dotnetcore.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

var builder = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration(configuration => configuration.AddCommandLine(args))
    .ConfigureServices(services =>
    {
        services.AddLogging(cfg => cfg.AddConsole());
        services.AddHostedService<Workflow>();
    });

var app = builder.Build();
app.Run();
