using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CommandLine;
using dotnetcore.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace dotnetcore.Services;

public class Workflow : BackgroundService
{
    private readonly ILogger _logger;
    private readonly IHostApplicationLifetime _hostLifetime;
    public Workflow(ILogger<Workflow> logger, IHostApplicationLifetime hostLifetime)
    {
        _logger = logger;
        _hostLifetime = hostLifetime;
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("dotnet.homework.23 async file read");
        
        var commandLineOptions = Parser.Default.ParseArguments<CommandLineArguments>(Environment.GetCommandLineArgs()).Value;
        if (string.IsNullOrEmpty(commandLineOptions.Folder))
        {
            _logger.LogWarning("you must to specify the folder");
            _hostLifetime.StopApplication();
        }
        
        if (!Directory.Exists(commandLineOptions.Folder))
        {
            _logger.LogWarning("directory not found");
            _hostLifetime.StopApplication();
        }

        var tasks = new List<Task>();
        var results = new List<OperationResult>();
        foreach (var file in Directory.GetFiles(commandLineOptions.Folder))
        {
            tasks.Add(
                await Task.Factory.StartNew<Task<OperationResult>>(
                        async state =>
                        {
                            if (state is not string filePath)
                                throw new ArgumentNullException(nameof(state));

                            using var streamReader = new StreamReader(filePath);
                            var data = await streamReader.ReadToEndAsync(stoppingToken);
                            return new OperationResult(filePath, data.Count(c => c is ' '));
                        }, file, stoppingToken)
                    .ContinueWith(async t => results.Add(await t.Result), stoppingToken));
        }
        await Task.WhenAll(tasks);
        
        Console.WriteLine("Result\n");
        foreach (var result in results)
            Console.WriteLine(result);
    }
}