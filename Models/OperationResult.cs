using System.IO;

namespace dotnetcore.Models;

public class OperationResult
{
    public OperationResult(string filePath, int numberOfSpaces)
    {
        FilePath = filePath;
        NumberOfSpaces = numberOfSpaces;
    }

    private string FilePath { get; }
    private int NumberOfSpaces { get; }

    public override string ToString()
        => $"file {Path.GetFileName(FilePath)} number of space characters: {NumberOfSpaces}";
}