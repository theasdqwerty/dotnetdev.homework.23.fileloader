using CommandLine;

namespace dotnetcore.Models;

public class CommandLineArguments
{
    [Option('d', "folder", Required = true, HelpText = "folder path")]
    public string Folder { get; set; } = string.Empty;
}